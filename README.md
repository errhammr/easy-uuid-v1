# Easy UUIDv1 (and v6) in Rust

Easily create UUIDs in version 1 or 6.

Also provides a method to convert an UUIDv1 into an UUIDv6.

## Project status

Experimental

Help wanted! I'm new to Rust. If you have suggestions on how to improve the code feel free to open an issue or create a pull request.

Especially helpful would be help on

* documentation
* tests
* improving the ease of integrating this project as dependency into other projects


mod lib;

use lib::EasyUuid;

fn main() {
    let eu = EasyUuid::init([1, 2, 3, 4, 5, 6]);
    println!("{}", eu.uuid6());
}

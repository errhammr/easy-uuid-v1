use std::time::SystemTime;
use rand::Rng;
use uuid::Uuid;
use uuid::v1::{Context, Timestamp};

pub struct EasyUuid {
    ctx: Context,
    node_id: [u8; 6]
}

impl EasyUuid {
    pub fn init(node_id: [u8; 6]) -> Self {
        let mut rng = rand::thread_rng();
        let ctx = Context::new(rng.gen());
        EasyUuid {
            ctx: ctx,
            node_id: node_id
        }
    }

    pub fn uuid1(&self) -> Uuid {
        let duration = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap();
        let ts = Timestamp::from_unix(
            &self.ctx,
            duration.as_secs(),
            duration.subsec_nanos()
        );
        let uuid = Uuid::new_v1(ts, &self.node_id).unwrap();
        uuid
    }

    pub fn uuid6(&self) -> Uuid {
        Self::uuid1to6(self.uuid1()).unwrap()
    }

    pub fn uuid1to6(uuid1: Uuid) -> Option<Uuid> {
        if uuid1.as_bytes()[6] >> 4 != 1 {
            // this is not a version 1 uuid
            // fail early
            return None;
        }
        
        let mut bytes6: [u8; 16] = [0; 16];
        
        bytes6[ 0] = uuid1.as_bytes()[6] << 4 | uuid1.as_bytes()[7] >> 4;
        bytes6[ 1] = uuid1.as_bytes()[7] << 4 | uuid1.as_bytes()[4] >> 4;
        bytes6[ 2] = uuid1.as_bytes()[4] << 4 | uuid1.as_bytes()[5] >> 4;
        bytes6[ 3] = uuid1.as_bytes()[5] << 4 | uuid1.as_bytes()[0] >> 4;
        bytes6[ 4] = uuid1.as_bytes()[0] << 4 | uuid1.as_bytes()[1] >> 4;
        bytes6[ 5] = uuid1.as_bytes()[1] << 4 | uuid1.as_bytes()[2] >> 4;
        bytes6[ 6] = 0x10 | (uuid1.as_bytes()[2] & 0x0F);
        bytes6[ 7] = uuid1.as_bytes()[3];
        bytes6[8..].clone_from_slice(&uuid1.as_bytes()[8..]);
        
        
        Some(Uuid::from_bytes(bytes6))
    }
}
